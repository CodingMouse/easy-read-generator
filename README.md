## Easy-Read Generator
Generate formatted html files that are easier to read!

## Description
This project is a Python-based application designed to assist individuals with dyslexia in reading text files more easily. The application takes a text file as input and generates a formatted HTML file that is more accessible and easier to read for people with dyslexia. 

The application is customizable, allowing users to adjust the formatting to their liking and comfort. This feature ensures that the application caters to a wide range of reading preferences and needs.

## Usage
**OPTION 1 (through python)**
    To use the program you can simply download all python files, and run either the main-console.py, which runs on the console, or the main-ui, which runs on a ui.

        - To run main-console, no additional steps are needed, just run the file and you can start using it.

        - To run main-ui, first you need to make sure that you have the TKinter and CustomTKinter libraries installed, which can easily be installed through the terminal with the pip command.

**OPTION 2 (Windows users only)**
    Download the exe file, and double click it to run.

**How to use:**
It is very intuitive, but regardless, here are the steps:
1. Select a txt file
2. Set the format to your liking, or just leave it as default
3. Generate the file
4. Open the file in the newly generated "outputs" folder

Regardless of which file you decide to run, there's a few things to note:

    1. The input can only be a txt file
    
    2. After running, the program will create an "outputs" folder, which will be created in the same location where the program was run
