from my_functions import *


def main():
    # Default values
    size = '30'
    letter_spacing = '0.4'
    line_spacing = '1.8'

    while True:
        menu()
        ans = input(' > TYPE YOUR SELECTION:\n > ')

        if ans == 4 or ans == '4' or ans.lower() == 'exit' or ans.lower() == 'quit':
            break

        if ans == '1':
            clear()
            file_name = input(' > ENTER THE FILE PATH:\n > ')

            my_file = Path(file_name)

            if my_file.is_file():
                clear()

                # read file and store in string
                text = file_to_string(open(my_file))

                # call function that handles the generation with string 'text'
                generate_file(text, size, letter_spacing, line_spacing)

                print(' > SUCCESFULLY CREATED FILE')
                input(' > PRESS ANY KEY TO CONTINUE:\n')
                clear()
            else:
                clear()
                print(' > ERROR: FILE NOT FOUND, PLEASE MAKE SURE THAT YOU TYPED THE PATH CORRECTLY')
                input(' > PRESS ANY KEY TO CONTINUE:\n')
                clear()
        
        elif ans == '2':
            clear()
            # input a string
            text = input(' > ENTER THE TEXT TO CONVERT:\n > ')

            clear()

            # call function that handles the generating with previous string
            generate_file(text, size, letter_spacing, line_spacing)


            print(' > SUCCESFULLY CREATED FILE')
            input(' > PRESS ANY KEY TO CONTINUE:\n')
            clear()
        
        elif ans == '3':
            clear()

            size = input(' > ENTER THE FONT SIZE (DEFAULT IS 30):\n > ')
            print()

            letter_spacing = input(' > ENTER THE LETTER-SPACING (DEFAULT IS 0.4):\n > ')
            print()

            line_spacing = input(' > ENTER THE LINE-SPACING (DEFAULT IS 1.8):\n > ')
            print()

            clear()
        
        else:
            clear()
            print(' > ERROR: INVALID SELECTION, PLEASE TRY AGAIN')
            input(' > PRESS ENTER TO CONTINUE:\n')
            clear()


if __name__ == "__main__":
    main()