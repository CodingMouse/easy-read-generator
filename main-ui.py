# import tkinter
# from tkinter import ttk
# from tkinter import messagebox

import customtkinter as ctk
from customtkinter import filedialog

from tkinter import messagebox

import my_functions as erg

args = {
    'file': '',
    'font-size': '30',
    'letter-spacing': '0.4',
    'line-spacing': '1.8'
}

font_type = 'consolas'
font_size = 28


def validate():
    if args['file'] == '':
        messagebox.showerror(title="Error", message="Please select a file.")
        return

    fontSize = fontSizeEntry.get()
    if fontSize == "" or not fontSize.replace(' ','').replace(' ','').replace('.', '', 1).isdigit():
        # print("Please enter a valid font size.")
        messagebox.showerror(title="Error", message="Font-size is invalid, please enter a valid font, or leave it as default.")
        return
    letterSpacing = letterSpacingEntry.get()
    if letterSpacing == "" or not letterSpacing.replace(' ','').replace('.', '', 1).isdigit():
        # print("Please enter a valid font size.")
        messagebox.showerror(title="Error", message="Letter-spacing is invalid, please enter a valid value, or leave it as default.")
        return
    lineSpacing = lineSpacingEntry.get()
    if lineSpacing == "" or not lineSpacing.replace(' ','').replace('.', '', 1).isdigit():
        # print("Please enter a valid font size.")
        messagebox.showerror(title="Error", message="Line-spacing is invalid, please enter a valid value, or leave it as default.")
        return
    
    text = erg.file_to_string(open(args['file']))
    # print(text)
    args['font-size'] = fontSize.replace(' ','')
    args['letter-spacing'] = letterSpacing.replace(' ','')
    args['line-spacing'] = lineSpacing.replace(' ','')

    erg.generate_file(text, args['font-size'], args['letter-spacing'], args['line-spacing'])
    messagebox.showinfo(title="Success", message="File has been generated.")



def defaultSettings():
    global args
    args['font-size'] = 30
    args['letter-spacing'] = 0.4
    args['line-spacing'] = 1.8

    fontSizeEntry.delete(0, ctk.END)
    letterSpacingEntry.delete(0, ctk.END)
    lineSpacingEntry.delete(0, ctk.END)

    fontSizeEntry.insert(0, 30)
    letterSpacingEntry.insert(0, 0.4)
    lineSpacingEntry.insert(0, 1.8)
    


def open_file():
    filename = filedialog.askopenfilename()
    if filename == "":
        return
    args['file'] = filename
    # print(args)
    # print(filename)  # prints the path of the selected file



##################################################################################################################
# UI

ctk.set_appearance_mode("dark")

# ctk.set_default_color_theme("blue")
ctk.set_default_color_theme("dark-blue")
# ctk.set_default_color_theme("green")

window = ctk.CTk()
window.title("Easy-Read Generator")
window.geometry("800x450")



frame = ctk.CTkFrame(window)
frame.pack(padx=40, pady=40, fill='both')



setupFrame = ctk.CTkFrame(frame)
setupFrame.pack(padx=20, pady=20, fill='both')

# Configure the columns and rows to expand
rowMINSIZE = 30
setupFrame.grid_columnconfigure(0, weight=1)
setupFrame.grid_columnconfigure(1, weight=1)
setupFrame.grid_rowconfigure(0, weight=1, minsize=rowMINSIZE)
setupFrame.grid_rowconfigure(1, weight=1, minsize=rowMINSIZE)
setupFrame.grid_rowconfigure(2, weight=1, minsize=rowMINSIZE)
setupFrame.grid_rowconfigure(3, weight=1, minsize=rowMINSIZE)
setupFrame.grid_rowconfigure(4, weight=1, minsize=rowMINSIZE)



setupFrame_PADY = 30
selButton = ctk.CTkButton(setupFrame, text="SELECT FILE", command=open_file, font=(font_type, font_size))
selButton.grid(row=0, column=0, columnspan=2, sticky='ew')
fontSizeLabel = ctk.CTkLabel(setupFrame, text="FONT SIZE", font=(font_type, font_size, "bold"))
fontSizeLabel.grid(row=1, column=0, sticky='w')
letterSpacingLabel = ctk.CTkLabel(setupFrame, text="LETTER SPACING", font=(font_type, font_size, "bold"))
letterSpacingLabel.grid(row=2, column=0, sticky='w')
lineSpacingLabel = ctk.CTkLabel(setupFrame, text="LINE SPACING", font=(font_type, font_size, "bold"))
lineSpacingLabel.grid(row=3, column=0, sticky='w')
defButton = ctk.CTkButton(setupFrame, text="SET TO DEFAULT", command=defaultSettings, font=(font_type, font_size))
defButton.grid(row=4, column=0, columnspan=2, sticky='ew')

fontSizeEntry = ctk.CTkEntry(setupFrame, font=(font_type, font_size))
fontSizeEntry.grid(row=1, column=1, sticky='e', pady=setupFrame_PADY)
letterSpacingEntry = ctk.CTkEntry(setupFrame, font=(font_type, font_size))
letterSpacingEntry.grid(row=2, column=1, sticky='e', pady=setupFrame_PADY)
lineSpacingEntry = ctk.CTkEntry(setupFrame, font=(font_type, font_size))
lineSpacingEntry.grid(row=3, column=1, sticky='e', pady=setupFrame_PADY)

fontSizeEntry.insert(0, 30)
letterSpacingEntry.insert(0, 0.4)
lineSpacingEntry.insert(0, 1.8)



genButton = ctk.CTkButton(window, text="GENERATE FILE", command=validate, font=(font_type, font_size))
genButton.pack()


for widget in setupFrame.winfo_children():
    widget.grid_configure(padx=10, pady=5)

window.mainloop()
