from pathlib import Path
import os

def file_to_string(my_file):
    return my_file.read().replace('\n', ' <br> ')



def generate_file(input_text, size = '30', letter_spacing = '0.4', line_spacing = '1.8'):
    # clear()
    # print(' > GENERATING FILE...\n')

    if not os.path.exists('outputs'):
        os.makedirs('outputs')
        
    outputNAME = 'outputs/output.html'
    my_file = Path(outputNAME)

    i = 1
    while my_file.is_file():
        outputNAME = 'outputs/output({}).html'.format(i)
        my_file = Path(outputNAME)
        i += 1
    
    output = open(outputNAME, "w")

    text = input_text.split()

    output.write('<!DOCTYPE html>\n')
    output.write('<html lang="en">\n')

    output.write('\t<head>\n')
    output.write('\t\t<title>Easy Read Output</title>\n\t\t<meta charset="UTF-8">\n\t\t<meta name="viewport" content="width=device-width, initial-scale=1">\n\t\t<link href="css/style.css" rel="stylesheet">\n')
    output.write('\t</head>\n')

    output.write('\t<body>\n')

    if not size.replace('.', '', 1).isdigit():
        size = 30
        print(' > ERROR: FONT SIZE IS NOT VALID\n > DEFAULT SIZE OF 30 WILL BE APPLIED.')
        # input(' > PRESS ANY KEY TO CONTINUE:\n')

    output.write('\t\t<p style="font-size: {}pt; color:DodgerBlue;" >'.format(size))

    for word in text:
        output.write('<b style="font-size: {}pt;  color:blue;">'.format(float(size) + 1.5))

        if word == '<br>':
            output.write(word + '</b>')
            continue

        check = False

        if len(word) != 1:
            for i in range(len(word)):
                if not check and i > (len(word)//5):
                    check = True
                    output.write('</b>')
                output.write(word[i])
        
        else:
            output.write(word + '</b>')
        
        output.write('&ensp;&ensp;')
    output.write('</p>\n')

    output.write('\t</body>\n')

    if not letter_spacing.replace('.', '', 1).isdigit():
        letter_spacing = 0.4
        print(' > ERROR: LETTER SPACING IS NOT VALID\n > DEFAULT SIZE OF 0.4 WILL BE APPLIED.')
        # input(' > PRESS ANY KEY TO CONTINUE:\n')

    if not line_spacing.replace('.', '', 1).isdigit():
        line_spacing = 1.8
        print(' > ERROR: LINE SPACING IS NOT VALID\n > DEFAULT SIZE OF 1.8 WILL BE APPLIED.')
        # input(' > PRESS ANY KEY TO CONTINUE:\n')

    output.write('\t<style>\n\t\tp {{\n\t\t\tletter-spacing: {}rem;\n\t\t\tline-height: {};\n\t\t}}\n\t</style>\n'.format(letter_spacing, line_spacing))

    output.write('</html>\n')

    output.close()



####################################################################################################################################################
####################################################################################################################################################
####################################################################################################################################################
def clear():
    os.system('cls' if os.name == 'nt' else 'clear')



def menu():
    print()
    print(' ---------------EASY-READ-GENERATOR---------------')
    print()
    print(' 1. GENERATE FROM FILE')
    print(' 2. GENERATE FROM TEXT')
    print(' 3. SETTINGS')
    print()
    print(' 4. EXIT')
    print()
    print(' -------------------------------------------------')
    print()